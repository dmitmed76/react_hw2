import { Component } from 'react';
import PropTypes from 'prop-types';

import { CardsWrapper, CardWrapper } from './styledGoodsCards'
import { Card } from './components/Card';


class GoodsCards extends Component {

	render() {
		const { projectors, openModal, hendlerCurrentGoods, hendlerFavorites, toggleIcons } = this.props;

		return (
			<CardsWrapper>
				{projectors.map((el, index) => (
					<CardWrapper key={index}>
						<Card
							pos={index}
							item={el}
							hendlerFavorites={hendlerFavorites}
							openModal={openModal}
							hendlerCurrentGoods={hendlerCurrentGoods}
							toggleIcons={toggleIcons}
							isFavorite={el.isFavorite}
						/>
					</CardWrapper>
				))}

			</CardsWrapper>
		);
	}
}
GoodsCards.propTypes = {
	projectors: PropTypes.array,
	hendlerCurrentGoods: PropTypes.func,
	hendlerFavorites: PropTypes.func,
	openModal: PropTypes.func
}

export { GoodsCards };

