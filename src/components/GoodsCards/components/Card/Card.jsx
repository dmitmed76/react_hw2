import { Component } from 'react';
import PropTypes from 'prop-types';

import {
	CardTitle, GoodsColor, CardArt,
	CardImg, CardPrice, CardStar, ButtonStyle
} from './styledCard';
import { ReactComponent as FavoritStar } from './icons/favoritStar.svg';

class Card extends Component {

	render() {
		const { openModal, hendlerCurrentGoods, hendlerFavorites, item, toggleIcons, pos, isFavorite } = this.props;
		const starFill = isFavorite ? "#1e14a7" : "#8f8989";

		return (
			<>
				<CardTitle>{this.props.item.title}</CardTitle>
				<GoodsColor>Колір: {this.props.item.color}</GoodsColor>
				<CardArt>Арт.: {this.props.item.article}</CardArt>
				<CardImg src={this.props.item.url} alt={this.props.item.title} />
				<CardPrice>Ціна: {this.props.item.price} грн.</CardPrice>
				<CardStar type='button' onClick={() => {
					hendlerFavorites(item)
					toggleIcons(pos)
				}}>
					<FavoritStar style={{ fill: starFill }} />
				</CardStar>
				<ButtonStyle type='button' onClick={() => {
					openModal()
					hendlerCurrentGoods(item)
				}}> Add to cart </ ButtonStyle>
			</>
		);
	}
}

Card.propTypes = {
	openModal: PropTypes.func,
	hendlerCurrentGoods: PropTypes.func,
	hendlerFavorites: PropTypes.func,
	item: PropTypes.object
}

export { Card, ButtonStyle };



