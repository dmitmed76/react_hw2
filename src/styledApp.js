import styled from 'styled-components';
import { ButtonStyle } from './components/GoodsCards/components/Card';

const AppWrapper = styled.div``;

const ButtonModalWrapper = styled.div`
display: flex;
justify-content: space-around;
`
const ButtonModal = styled(ButtonStyle)`
background-color: ${props => props.backgroundColor || 'Green'};
width: 150px;
`
export { AppWrapper, ButtonModalWrapper, ButtonModal };