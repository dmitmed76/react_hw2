import { Component } from "react";

import { AppWrapper, ButtonModalWrapper, ButtonModal } from "./styledApp";
import { GoodsCards } from "./components/GoodsCards";
import { Modal } from "./components/Modal";
import { Header } from "./components/Header";

class App extends Component {
  state = {
    isModal: false,
    projectors: [],
    basket: JSON.parse(localStorage.getItem("basket")) || [],
    favorites: JSON.parse(localStorage.getItem("favorites")) || [],
    currentGoods: {},
    goods: {},
  };

  componentDidMount() {
    fetch("goods.json")
      .then((res) =>{console.log(res);return res.json();})
      
      .then((result) => {
        this.setState({
          projectors: JSON.parse(localStorage.getItem("favoritIcon")) || result,
        });
      });
  }

  toggleIcons = (pos) => {
    const temp = [...this.state.projectors];
    temp[pos].isFavorite = !temp[pos].isFavorite;

    this.setState({
      projectors: temp,
    });
    localStorage.setItem("favoritIcon", JSON.stringify(temp));
  };

  hendlerModal = () => {
    this.setState((prevState) => {
      return {
        ...prevState,
        isModal: !prevState.isModal,
      };
    });
  };

  hendlerCurrentGoods = (currentGoods) => {
    this.setState((prevState) => {
      return {
        ...prevState,
        currentGoods: { ...currentGoods },
      };
    });
  };

  hendlerFavorites = (goods) => {
    this.setState((prevState) => {
      if (!prevState.favorites.includes(goods)) {
        return {
          ...prevState,
          favorites: [...prevState.favorites, goods],
        };
      } else {
        return {
          ...prevState,
          favorites: prevState.favorites.filter((item) => item !== goods),
        };
      }
    });
  };

  hendlerBasket = (currentGoods) => {
    this.setState((prevState) => {
      return {
        ...prevState,
        isModal: !prevState.isModal,
        basket: [...prevState.basket, currentGoods],
      };
    });
  };

  render() {
    const { isModal, basket, currentGoods, favorites } = this.state;
    localStorage.setItem("basket", JSON.stringify(basket));
    localStorage.setItem("favorites", JSON.stringify(favorites));

    const actions = (
      <ButtonModalWrapper>
        <ButtonModal
          type="button"
          onClick={() => this.hendlerBasket(currentGoods)}
        >
          ok
        </ButtonModal>
        <ButtonModal
          type="button"
          backgroundColor={"#B43727"}
          onClick={this.hendlerModal}
        >
          cancel
        </ButtonModal>
      </ButtonModalWrapper>
    );

    return (
      <AppWrapper>
        <Header countBasket={basket.length} countFavorite={favorites.length} />

        <GoodsCards
          projectors={this.state.projectors}
          openModal={this.hendlerModal}
          hendlerCurrentGoods={this.hendlerCurrentGoods}
          hendlerFavorites={this.hendlerFavorites}
          toggleIcons={this.toggleIcons}
        />

        {isModal && (
          <Modal
            actions={actions}
            closeButton={this.hendlerModal}
            header="Хочете додати цей товар в корзину?"
            text="Натисніть 'ОК' для додавання товару в корзину або натисніть 'CANCEL' для відміни "
            backgroundColor="lightblue"
            color="white"
          />
        )}
      </AppWrapper>
    );
  }
}

export default App;
